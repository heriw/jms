#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use DBI;
my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $sth = $dbh->prepare( "SELECT  id, nama FROM ekspedisi" );  
$sth->execute();

print "Content-Type: text/html\n\n";
print '<!DOCTYPE html>
<html>
<head>
<title>JMS | Surat Jalan | Tambah</title>
<style type="text/css">
	.judulkolom{background-color: #5e5e5e;color: #fff; font-weight: bold;}
	body{background-color: #e5e5e5;}
	table{border-collapse: collapse; width: 100%;}
	table td {padding: 3px;}
	img {
	    vertical-align: text-top;
	}
	a {text-decoration: none;color: #f5f5f5; font-size: smaller; }
</style>
</head>
<body>
<h1>Tambah Surat Jalan</h1>
<form action="suratjalanaddact.pl" method="post" enctype="multipart/form-data">
<table>
	<tr><td>Tanggal</td><td><input type="date" name="tgl_surat" value="" autofocus required></td></tr>
	<tr><td>Nomor</td> <td><input type="text" name="nomor" required></td></tr>
	<tr><td>Nomor Order</td><td><input type="text" name="nomor_order" required></td></tr>
	<tr><td>Ekspedisi</td><td><select name="ekspedisi" resuired>';
	while (my @data = $sth->fetchrow_array()) {
	    print '<option value="'.$data[0].'">'.$data[1].'</option>';
	} 
print '</select></td></tr>
	<tr><td>Purchase</td><td><input type="text" name="org_purchase" required></td></tr>
	<tr><td>Yang Menerima</td><td><input type="text" name="penerima"></td></tr>
	<tr><td>Yang Menyerahkan</td><td><input type="text" name="penyerah"></td></tr>
</table>
<hr>
<table>
<tr><td>No</td><td>Kode</td><td>Nama</td><td>Jumlah</td></tr>
<tr><td>1</td><td><input type="text" id="kode_barang1" name="kode_barang1" required onchange="cariBarangDariKode(this.value,1)" ></td><td><input type="text" id="nama_barang1" name="nama_barang1"  onchange="cariBarangDariNama(this.value,1)" ></td><td><input type="text" id="jml_barang1" name="jml_barang1" ></td></tr>
<tr><td>2</td><td><input type="text" id="kode_barang2" name="kode_barang2"></td><td><input type="text" id="nama_barang2" name="nama_barang2" ></td><td><input type="text" id="jml_barang2" name="jml_barang2" ></td></tr>
<tr><td>3</td><td><input type="text" id="kode_barang3" name="kode_barang3" ></td><td><input type="text" id="nama_barang3" name="nama_barang3" ></td><td><input type="text" id="jml_barang3" name="jml_barang3" ></td></tr>
<tr><td>4</td><td><input type="text" id="kode_barang4" name="kode_barang4" ></td><td><input type="text" id="nama_barang4" name="nama_barang4" ></td><td><input type="text" id="jml_barang4" name="jml_barang4" ></td></tr>
<tr><td>5</td><td><input type="text" id="kode_barang5" name="kode_barang5" ></td><td><input type="text" id="nama_barang5" name="nama_barang5" ></td><td><input type="text" id="jml_barang5" name="jml_barang5" ></td></tr>
<tr><td>6</td><td><input type="text" id="kode_barang6" name="kode_barang6" ></td><td><input type="text" id="nama_barang6" name="nama_barang6" ></td><td><input type="text" id="jml_barang6" name="jml_barang6" ></td></tr>
<tr><td>7</td><td><input type="text" id="kode_barang7" name="kode_barang7" ></td><td><input type="text" id="nama_barang7" name="nama_barang7" ></td><td><input type="text" id="jml_barang7" name="jml_barang7" ></td></tr>
<tr><td>8</td><td><input type="text" id="kode_barang8" name="kode_barang8" ></td><td><input type="text" id="nama_barang8" name="nama_barang8" ></td><td><input type="text" id="jml_barang8" name="jml_barang8" ></td></tr>
<tr><td>9</td><td><input type="text" id="kode_barang9" name="kode_barang9" ></td><td><input type="text" id="nama_barang9" name="nama_barang9" ></td><td><input type="text" id="jml_barang9" name="jml_barang9" ></td></tr>
<tr><td>10</td><td><input type="text" id="kode_barang10" name="kode_barang10" ></td><td><input type="text" id="nama_barang10" name="nama_barang10" ></td><td><input type="text" id="jml_barang10" name="jml_barang10" ></td></tr>
</table>
<hr>
<table>
	<tr><td><input type="reset" name="reset" value="Bersih"></td><td><input type="submit" name="submit" value="Rekam"></td></tr>
	</table>
</form>
</body>
</html>
';