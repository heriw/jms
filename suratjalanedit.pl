#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use CGI qw(:standard);
use DBI;
my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $id=param('id');
my $sth = $dbh->prepare( "SELECT  id, nama FROM ekspedisi" );  
$sth->execute();

my $suratjalan = $dbh->prepare( "SELECT tgl_surat, org_purchase, nomor, nomor_order, penerima, penyerah, id_ekspedisi FROM suratjalan WHERE id=$id" );  
$suratjalan->execute();
my @datasuratjalan = $suratjalan->fetchrow();

my $suratjalanbarang = $dbh->prepare( "SELECT `id`, `id_suratjalan`, `kode_barang`, `nama_barang`, `jml_barang` FROM `suratjalanbarang` WHERE id_suratjalan=$id" );  
$suratjalanbarang->execute();


print "Content-Type: text/html\n\n";
print '<!DOCTYPE html>
<html>
<head>
<title>JMS | Surat Jalan | Ubah</title>
<style type="text/css">
	.judulkolom{background-color: #5e5e5e;color: #fff; font-weight: bold;}
	body{background-color: #e5e5e5;}
	table{border-collapse: collapse; width: 100%;}
	table td {padding: 3px;}
	img {
	    vertical-align: text-top;
	}
	a {text-decoration: none;color: #f5f5f5; font-size: smaller; }
</style>
</head>
<body>
<h1>Ubah Surat Jalan</h1>
<form action="suratjalaneditact.pl" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="'.$id.'" />
<table>
	<tr><td>Tanggal</td><td><input type="date" name="tgl_surat" value="'.$datasuratjalan[0].'" autofocus required></td></tr>
	<tr><td>Nomor</td> <td><input type="text" name="nomor" value="'.$datasuratjalan[2].'" required></td></tr>
	<tr><td>Nomor Order</td><td><input type="text" name="nomor_order"  value="'.$datasuratjalan[3].'" required></td></tr>
	<tr><td>Ekspedisi</td><td><select name="ekspedisi" resuired>';
	while (my @data = $sth->fetchrow_array()) {
		if ($datasuratjalan[6] == $data[0]) {
			print '<option value="'.$data[0].'" selected="selected" >'.$data[1].'</option>';
		} else {
			print '<option value="'.$data[0].'" >'.$data[1].'</option>';
		}	    
	} 
print '</select></td></tr>
	<tr><td>Purchase</td><td><input type="text" name="org_purchase"  value="'.$datasuratjalan[1].'"  required></td></tr>
	<tr><td>Yang Menerima</td><td><input type="text" name="penerima"  value="'.$datasuratjalan[4].'" ></td></tr>
	<tr><td>Yang Menyerahkan</td><td><input type="text" name="penyerah"  value="'.$datasuratjalan[5].'" ></td></tr>
</table>
<hr>
<table>
<tr><td>No</td><td>Kode</td><td>Nama</td><td>Jumlah</td></tr>';
my $i=1;
while (my @databarang = $suratjalanbarang->fetchrow_array()) {
	print '<tr><td>1</td><td>
	<input type="text" id="kode_barang'.$i.'" name="kode_barang'.$i.'" value="'.$databarang[2].'" ></td><td>
	<input type="text" id="nama_barang'.$i.'" name="nama_barang'.$i.'" value="'.$databarang[3].'"  ></td><td>
	<input type="text" id="jml_barang'.$i.'" name="jml_barang'.$i.'"  value="'.$databarang[4].'" ></td></tr>';    
	$i=$i+1;
}
if ($i<11) {
	for (my $k=$i; $k<=10; $k++ ) {
		print '<tr><td>'.$k.'</td><td>
		<input type="text" id="kode_barang'.$k.'" name="kode_barang'.$k.'" ></td><td>
		<input type="text" id="nama_barang'.$k.'" name="nama_barang'.$k.'" ></td><td>
		<input type="text" id="jml_barang'.$k.'" name="jml_barang'.$k.'" ></td></tr>';	
	}
}
print '
</table>
<hr>
<table>
	<tr><td><input type="reset" name="reset" value="Bersih"></td><td><input type="submit" name="submit" value="Rekam"></td></tr>
	</table>
</form>
</body>
</html>
';