#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use CGI qw(:standard);
use DBI;
my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;
my $id=param('id');
my $sth = $dbh->prepare( "SELECT  id, nama, alamat, kota, telepon FROM ekspedisi WHERE id=$id" );  
$sth->execute();
my @data = $sth->fetchrow();
print "Content-Type: text/html\n\n";
print '<!DOCTYPE html>
<html>
<head>
<title>JMS | Ekspedisi| Ubah</title>
<style type="text/css">
	.judulkolom{background-color: #5e5e5e;color: #fff; font-weight: bold;}
	body{background-color: #e5e5e5;}
	table{border-collapse: collapse; width: 100%;}
	table td {padding: 3px;}
	img {
	    vertical-align: text-top;
	}
	a {text-decoration: none;color: #f5f5f5; font-size: smaller; }
</style>
</head>
<body>
<h1>Ubah Ekspedisi</h1>
<form action="perusahaaneditact.pl" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="'.$data[0].'" />
<table>
	<tr><td>Nama</td><td><input type="text" name="nama" required="" autofocus="" value="'.$data[1].'"></td></tr>
	<tr><td>Alamat</td><td><textarea name="alamat" rows="4" cols="20" required="">'.$data[2].'</textarea></td></tr>
	<tr><td>Kota</td><td><input type="text" name="kota" required="" value="'.$data[3].'"></td></tr>
	<tr><td>Telepon</td><td><input type="tel" name="telepon" required="" value="'.$data[4].'"></td></tr>
	<tr><td><input type="reset" name="reset" value="Bersih"></td><td><input type="submit" name="submit" value="Rekam"></td></tr>
</table>
</form>
</body>
</html>';