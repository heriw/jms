#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use DBI;
use CGI qw(:standard);;
my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $sth = $dbh->prepare( "SELECT  id, nama, alamat, kota, telepon FROM ekspedisi" );  
$sth->execute();

print "Content-Type: text/html\n\n";
print '<!DOCTYPE html>
<html>
<head>
<title>JMS | Ekspedisi| List</title>
</head>

<body>
<h1>Daftar Ekspedisi</h1>
<table border="1" width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
	<thead>
	<tr class="judulkolom" ><th>Perusahaan</th><th>Alamat</th><th>Kota</th><th>Telepon</th></tr>
	</thead>
	<tbody>';	
while (my @data = $sth->fetchrow_array()) {
	print '<tr><td>'.$data[1].'</td><td>'.$data[2].'</td><td>'.$data[3].'</td><td>'.$data[4].'</td>
	</tr>';
}

$sth->finish();
$dbh->disconnect();

print '</tbody>
</table>

</body>
</html>';
