#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use CGI qw(:standard);
use DBI;

my $id=param('id');

my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;


$dbh->do("DELETE FROM ekspedisi WHERE id=".$id);

$dbh->disconnect();


my $cgi = new CGI;

print $cgi->header(-location => q[http://localhost/jms/index.pl]);