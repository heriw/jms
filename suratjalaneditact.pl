#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use CGI qw(:standard);
use DBI;

my $id=param('id');
my $tgl_surat = param('tgl_surat');
my $nomor = param('nomor');
my $nomor_order = param('nomor_order');
my $ekspedisi = param('ekspedisi');
my $org_purchase = param('org_purchase');
my $nomor = param('nomor');
my $penerima = param('penerima');
my $penyerah = param('penyerah');

my @kode_barang;
my @nama_barang;
my @jml_barang;
for (my $var = 1; $var < 11; $var++) {
	$kode_barang[$var]=param('kode_barang'.$var);
	$nama_barang[$var]=param('nama_barang'.$var);
	$jml_barang[$var]=param('jml_barang'.$var);
}

my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;


$dbh->do("UPDATE suratjalan SET tgl_surat='".$tgl_surat."', org_purchase='".$org_purchase."', nomor='".$nomor."', nomor_order='".$nomor_order."', penerima='".$penerima."', penyerah='".$penyerah."', id_ekspedisi=".$ekspedisi." 
WHERE id=$id");

$dbh->do("DELETE FROM suratjalanbarang WHERE id_suratjalan=$id");
for (my $i = 1; $i < 11; $i++){
	if( $kode_barang[$i] && $nama_barang[$i] && $jml_barang[$i]) {
		$dbh->do("INSERT INTO suratjalanbarang SET id_suratjalan=".$id.", kode_barang='".$kode_barang[$i]."', nama_barang='".$nama_barang[$i]."', jml_barang=".$jml_barang[$i]." ");
	}
}

$dbh->disconnect();

my $cgi = new CGI;

print $cgi->header(-location => q[http://localhost/jms/suratjalanlist.pl]);