#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use DBI;
my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $sth = $dbh->prepare( "SELECT  id, tgl_surat, org_purchase, nomor, penerima  FROM suratjalan" );  
$sth->execute();


print "Content-Type: text/html\n\n";
print '<!DOCTYPE html>
<html>
<head>
<title>JMS | Surat Jalan | List</title></head>
<style type="text/css">
.judulkolom{background-color: #5e5e5e;color: #fff; font-weight: bold;}
body{background-color: #e5e5e5;}
table{border-collapse: collapse; width: 100%;}
table td {padding: 3px;}
img {
    vertical-align: text-top;
}
a {text-decoration: none;color: #f5f5f5; font-size: smaller; }
.warnahitam{color:#5e5e5e;}
</style>
<body>
<h1>Daftar Surat Jalan</h1>
<table cellpadding="0">
	<tr class="judulkolom" ><td>Tanggal</td><td>Nomor</td><td>Purchase</td><td>Penerima</td><td><a href="suratjalanadd.pl"><img src="add.png" width="15" height="15" alt="Tambah Data Perusahaan"> Tambah</a></td></tr>';

while (my @data = $sth->fetchrow_array()) {
	print '<tr><td>'.$data[1].'</td><td>'.$data[2].'</td><td>'.$data[3].'</td><td>'.$data[4].'</td>
	<td><a href="http://localhost/jms/suratjalanedit.pl?id='.$data[0].'" class="warnahitam" >Ubah</a>
	| <a href="http://localhost/jms/suratjalandel.pl?id='.$data[0].'" class="warnahitam" >Hapus</a></td></tr>';
}

$sth->finish();
$dbh->disconnect();

print '</table>
</body>
</html>';
