#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use CGI qw(:standard);
use DBI;

my $nama = param('nama');
my $alamat = param('alamat');
my $kota = param('kota');
my $telepon = param('telepon');

my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;


$dbh->do("INSERT INTO ekspedisi SET nama='".$nama."', alamat='".$alamat."', kota='".$kota."', telepon='".$telepon."' ");

$dbh->disconnect();


my $cgi = new CGI;

print $cgi->header(-location => q[http://localhost/jms/index.pl]);
