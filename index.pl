#!/opt/lampp/bin/perl -w
use strict;
use warnings;
use DBI;
use CGI qw(:standard);;
my $dbh = DBI->connect(          
    "dbi:mysql:dbname=jms", 
    "root",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $sth = $dbh->prepare( "SELECT  id, nama, alamat, kota, telepon FROM ekspedisi" );  
$sth->execute();

print "Content-Type: text/html\n\n";
print '<!DOCTYPE html>
<html>
<head>
<title>JMS | Ekspedisi| List</title>
<style type="text/css">
.judulkolom{background-color: #5e5e5e;color: #fff; font-weight: bold;}
body{background-color: #e5e5e5;}
table{border-collapse: collapse; width: 100%;}
table td {padding: 3px;}
img {
    vertical-align: text-top;
}
a {text-decoration: none;color: #f5f5f5; font-size: smaller; }
.warnahitam{color:#5e5e5e;}
</style>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" >
<script src="https://code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script  type="text/javascript" >
$(document).ready(function() {
    $("#example").DataTable();
} );
</script>
</head>

<body>
<h1>Daftar Ekspedisi <a href="perusahaancetak.pl" style="background: #888 none repeat scroll 0 0;
border-radius: 4px;
float: right;
font-size: 14px;
padding: 4px 17px;" >Cetak</a></h1>
<table id="example" class="display" width="100%" cellspacing="0" cellpadding="0">
	<thead>
	<tr class="judulkolom" ><td>Perusahaan</td><td>Alamat</td><td>Kota</td><td>Telepon</td><td><a href="perusahaanadd.pl"><img src="add.png" width="15" height="15" alt="Tambah Data Perusahaan"> Tambah</a></td></tr>
	</thead>
	<tbody>
	';
	
while (my @data = $sth->fetchrow_array()) {
	print '<tr><td>'.$data[1].'</td><td>'.$data[2].'</td><td>'.$data[3].'</td><td>'.$data[4].'</td><td><a href="http://localhost/jms/perusahaanedit.pl?id='.$data[0].'" class="warnahitam" >Ubah</a>
	| <a href="http://localhost/jms/perusahaandel.pl?id='.$data[0].'" class="warnahitam" >Hapus</a></td></tr>';
}

$sth->finish();
$dbh->disconnect();

print '</tbody>
</table>

</body>
</html>';
