#!/opt/lampp/bin/perl -w
use strict;
use warnings;
print "Content-Type: text/html\n\n";
print '<!DOCTYPE html>
<html>
<head>
<title>JMS | Ekspedisi| Tambah</title>
<style type="text/css">
	.judulkolom{background-color: #5e5e5e;color: #fff; font-weight: bold;}
	body{background-color: #e5e5e5;}
	table{border-collapse: collapse; width: 100%;}
	table td {padding: 3px;}
	img {
	    vertical-align: text-top;
	}
	a {text-decoration: none;color: #f5f5f5; font-size: smaller; }
</style>
</head>
<body>
<h1>Tambah Ekspedisi</h1>
<form action="perusahaanaddact.pl" method="post" enctype="multipart/form-data">
<table>
	<tr><td>Nama</td><td><input type="text" name="nama" required="" autofocus=""></td></tr>
	<tr><td>Alamat</td><td><textarea name="alamat" rows="4" cols="20" required=""></textarea></td></tr>
	<tr><td>Kota</td><td><input type="text" name="kota" required=""></td></tr>
	<tr><td>Telepon</td><td><input type="tel" name="telepon" required=""></td></tr>
	<tr><td><input type="reset" name="reset" value="Bersih"></td><td><input type="submit" name="submit" value="Rekam"></td></tr>
</table>
</form>
</body>
</html>';